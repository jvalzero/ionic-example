import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';

import { Camera, CameraOptions } from '@ionic-native/camera';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

import { ListPage } from '../list/list';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  foto_url:any;
  clave:any;
  nombre:any;
  descripcion:any;
  precio:any;

  alerta_msg:string;

  constructor(public navCtrl: NavController, private alertCtrl: AlertController, private camera: Camera, private sqlite: SQLite) {

  }

  mostrarAlerta() {
    let alert = this.alertCtrl.create({
      title: 'Atención',
      subTitle: this.alerta_msg,
      buttons: ['Aceptar']
    });
    alert.present();
  }

  mostrarAlertaGuardado() {
    let alert = this.alertCtrl.create({
      title: 'Atención',
      subTitle: this.alerta_msg,
      buttons: [
        {
          text: 'Cerrar',
          handler: data => {
            this.foto_url = null;
            this.clave = null;
            this.nombre = null;
            this.descripcion = null;
            this.precio = null;
            this.alerta_msg = null;
          }
        },{
          text: 'Ver artículos',
          handler: data => {
            this.navCtrl.setRoot(ListPage);
          }
        }
      ]
    });
    alert.present();
  }

  tomarFoto() {
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
     this.foto_url = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      this.alerta_msg = 'Ocurrio un error.';
      this.mostrarAlerta();
    });
  }

  guardarArticulo() {
    if (this.foto_url == null) {
      this.alerta_msg = 'Favor de tomar la foto del artículo.';
      this.mostrarAlerta();
    } else if (this.clave == null) {
      this.alerta_msg = 'Favor de ingresar la clave del artículo.';
      this.mostrarAlerta();
    } else if (this.nombre == null) {
      this.alerta_msg = 'Favor de ingresar el nombre del artículo.';
      this.mostrarAlerta();
    } else if (this.descripcion == null) {
      this.alerta_msg = 'Favor de ingresar la descripción del artículo.';
      this.mostrarAlerta();
    } else if (this.precio == null) {
      this.alerta_msg = 'Favor de ingresar el precio del artículo.';
      this.mostrarAlerta();
    } else {
      this.sqlite.create({
        name: 'data.db',
        location: 'default'
      })
      .then((db: SQLiteObject) => {
        db.executeSql('INSERT INTO articulos (clave, nombre, descripcion, precio, foto) VALUES ("' + this.clave + '", "' + this.nombre + '", "' + this.descripcion + '", ' + this.precio + ', "' + this.foto_url + '")', {})
          .then(() => {
            console.log('Executed SQL');
            this.alerta_msg = 'El artículo se ha guardado con éxito.';
            this.mostrarAlertaGuardado();
          })
          .catch(e => {
            console.log(e);
            this.alerta_msg = e;
            this.mostrarAlerta();
          });
        })
      .catch(e => {
        console.log(e);
        this.alerta_msg = "error supremo";
        this.mostrarAlerta();
      });
    }
  }
}
