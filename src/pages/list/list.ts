import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ModalController } from 'ionic-angular';

import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

import { ModalPage } from '../modal/modal';

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
  alerta_msg:string;
  articulos:any = [];
  heroes:any = ['Windstorm', 'Bombasto', 'Magneta', 'Tornado'];

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, private alertCtrl: AlertController, private sqlite: SQLite) {
    this.CargarArticulos();
  }

  mostrarAlerta() {
    let alert = this.alertCtrl.create({
      title: 'Atención',
      subTitle: this.alerta_msg,
      buttons: ['Aceptar']
    });
    alert.present();
  }

  CargarArticulos() {
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    })
    .then((db: SQLiteObject) => {
      db.executeSql('SELECT * FROM articulos ORDER BY nombre ASC', {})
        .then((data) => {
          console.log('Executed SQL');
          for (let i = 0; i < data.rows.length; i++) {
            let item = data.rows.item(i);
            // do something with it
            // this.results.push(item);
            this.articulos.push(item);
          }
          // this.alerta_msg = "Registros: " + data.rows.length;
        })
        .catch(e => {
          this.alerta_msg = e;
          this.mostrarAlerta();
          console.log(e);
        });
      })
    .catch(e => {
      this.alerta_msg = 'error.';
      this.mostrarAlerta();
      console.log(e);
    });
  }

  mostrarDetalle(articulo) {
    // this.alerta_msg = "Artículo: " + articulo.nombre;
    // this.mostrarAlerta();
    let modal = this.modalCtrl.create(ModalPage, {
      nombre: articulo.nombre,
      clave: articulo.clave,
      foto: articulo.foto,
      descripcion: articulo.descripcion,
      precio: articulo.precio
    });
    modal.present();
  }
}
