import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-modal',
  templateUrl: 'modal.html',
})
export class ModalPage {
  clave:any;
  nombre:any;
  foto:any;
  descripcion:any;
  precio:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
    this.nombre = navParams.get('nombre');
    this.clave = navParams.get('clave');
    this.foto = navParams.get('foto');
    this.descripcion = navParams.get('descripcion');
    this.precio = navParams.get('precio');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalPage');
  }

  dismiss() {
   this.viewCtrl.dismiss();
 }
}
