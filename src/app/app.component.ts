import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any}>;

  alerta_msg:string;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, private alertCtrl: AlertController, private sqlite: SQLite) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Registro de Artículos', component: HomePage },
      { title: 'Listado de Artículos', component: ListPage }
    ];
  }

  mostrarAlerta() {
    let alert = this.alertCtrl.create({
      title: 'Atención',
      subTitle: this.alerta_msg,
      buttons: ['Aceptar']
    });
    alert.present();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.sqlite.create({
        name: 'data.db',
        location: 'default'
      })
      .then((db: SQLiteObject) => {
        db.executeSql('CREATE TABLE articulos(id INTEGER AUTO_INCREMENT, clave VARCHAR(32), nombre VARCHAR(50), descripcion VARCHAR(100), precio INTEGER, foto VARCHAR(250))', {})
          .then(() => {
            console.log('Executed SQL');
            // this.alerta_msg = 'Executed SQL';
            // this.mostrarAlerta();
          })
          .catch(e => {
            console.log(e);
            // this.alerta_msg = e;
            // this.mostrarAlerta();
          });
        })
      .catch(e => {
        console.log(e);
        this.alerta_msg = 'error';
        this.mostrarAlerta();
      });
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
