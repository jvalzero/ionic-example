# Ionic-example

Aplicación móvil android con Ionic, enfocada a la organización de archivos.

## Instalación

Instalar desde en un dispositivo android con versión 4.4 KitKat como mínimo (API 19) el archivo "app-debug.apk".

## Versiones

Se implementaron las siguientes versiones de software

```
Ionic 3.20.0
Cordova 8.0.0
```

## Plugins implementados

* [SQLite](https://ionicframework.com/docs/native/sqlite/) - Ionic Native SQLite
* [Camera](https://ionicframework.com/docs/native/camera/) - Ionic Native Camera

## Autor

* **Juan Valdez** - [jvalzero](https://github.com/jvalzero)
